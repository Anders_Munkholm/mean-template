import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule }    from '@angular/forms';
import {RouterModule} from '@angular/router';
import { AppComponent } from './app.component';
import { HeaderComponent } from './header/header.component';
import { ContentComponent } from './content/content.component';
import { LoginComponent } from './content/login/login.component';
import { AppRoutingModule } from './app-routing.module';

import { HttpClientModule } from '@angular/common/http';
import { RegisterComponent } from './content/register/register.component';
import { StorageService } from './storage/storage.service';
import { HeaderService } from './header/header.service';
import { LobbyComponent } from './content/lobby/lobby.component';
import { ChatComponent } from './content/lobby/chat/chat.component';
import { NgbModule} from '@ng-bootstrap/ng-bootstrap';
import { loginRoutingModule } from './login-routing.module';
import { AuthGuard } from './auth-guard.service';
import { AuthService } from './auth.service';

@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    ContentComponent,
    LoginComponent,

    RegisterComponent,

    LobbyComponent,

    ChatComponent,
    
  ],
  imports: [
    FormsModule,
    BrowserModule,
    HttpClientModule,
    NgbModule.forRoot(),
    AppRoutingModule,
    loginRoutingModule,
  ],
  providers: [StorageService,HeaderService,AuthGuard,AuthService],
  bootstrap: [AppComponent]
})
export class AppModule { }
