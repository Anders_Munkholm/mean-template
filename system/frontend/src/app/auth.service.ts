import { Injectable } from '@angular/core';

import { Observable } from 'rxjs/Observable';
import 'rxjs/add/observable/of';
import 'rxjs/add/operator/do';
import 'rxjs/add/operator/delay';
import { HttpClient } from '@angular/common/http/src/client';
import { User } from './model/User';
import { catchError, map, tap } from 'rxjs/operators';
@Injectable()
export class AuthService {
  isLoggedIn = false;

  // store the URL so we can redirect after logging in
  redirectUrl: string;


  login(loginUrl,user,httpOptions,http:HttpClient): Observable<any> {

    return http.post<any>(loginUrl,user,httpOptions)
    .pipe(
        tap(user => {
            var userO = user as User

            if (userO.token != null) {
                this.isLoggedIn = true;
            } 
        }),
      );
    
    
    // .subscribe(
    //     user => {
    //       this.storageService.User = user;
    //       this.headerService.changeLoginHeader(true);
          
    //     }
     
      
    //   );
  }

  logout(): void {
    this.isLoggedIn = false;
  }
}