import { Injectable } from '@angular/core';
import { Subject } from 'rxjs/Subject';
import { Observable } from 'rxjs/Observable';

@Injectable()
export class HeaderService {

  private subject: Subject<boolean> = new Subject<boolean>();
  private observable: Observable<boolean> = this.subject.asObservable();
  constructor() {


  }


  public loginNotification(): Observable<boolean> {
    return this.observable;
  }

  public changeLoginHeader(loggedIn: boolean) {
    this.subject.next(loggedIn);

  }



}
