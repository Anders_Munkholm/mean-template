import { async, ComponentFixture, TestBed,inject } from '@angular/core/testing';

import { HeaderComponent } from './header.component';
import { AuthService } from '../auth.service';
import { HeaderService } from './header.service';
import { StorageService } from '../storage/storage.service';
import { Router } from '@angular/router';
import { Subject } from 'rxjs/Subject';
import { Observable } from 'rxjs/Observable';
import { User } from '../model/User';

describe('HeaderComponent', () => {
  let component: HeaderComponent;
  let fixture: ComponentFixture<HeaderComponent>;
  var subject: Subject<boolean> = new Subject<boolean>();
  var observable: Observable<boolean> = subject.asObservable();
  let headerService = null;
  let storageService = null;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ HeaderComponent ],
      providers:[
        AuthService,
        HeaderService,
        StorageService,

        { provide: Router,      useClass: RouterStub }
      ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HeaderComponent);
    component = fixture.componentInstance;
    headerService = fixture.debugElement.injector.get(HeaderService);
    storageService = fixture.debugElement.injector.get(StorageService);
    fixture.detectChanges();

  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should subsribe to notifications on ngOnInit', () => {
    let spy = spyOn(headerService,"loginNotification").and.returnValue(observable)
    component.ngOnInit();
    expect(spy.calls.count()).toBe(1); 
  });

 

});


class RouterStub {
  navigateByUrl(url: string) { return url; }
}
