import { Component, OnInit } from '@angular/core';
import { HeaderService } from './header.service';
import { StorageService } from '../storage/storage.service';
import { Router } from '@angular/router';
import { AuthService } from '../auth.service';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {

  private username: string;
  public loggedIn: boolean = false;

  constructor(private authService : AuthService, private headerService: HeaderService, private storageService: StorageService, private router: Router) {

  }

  ngOnInit() {
    this.headerService.loginNotification().subscribe(
      loggedIn => {
        if (loggedIn) {
          this.username = this.storageService.User.username;
        }
        this.loggedIn = loggedIn;
      }
    )
  }


  logout() {
    this.storageService.User = null;
    this.headerService.changeLoginHeader(false);
    this.authService.logout()
    this.router.navigate(["/login"]);

  }

}
