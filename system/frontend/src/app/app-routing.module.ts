import { NgModule }             from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { LoginComponent }   from './content/login/login.component';
import { RegisterComponent } from './content/register/register.component';
import { LobbyComponent } from './content/lobby/lobby.component';



const appRoutes: Routes = [
    { 
        path: '',redirectTo: '/login', pathMatch: 'full' 
    },
    { 
        path: 'login', 
        component: LoginComponent
     },
     { 
        path: 'register', 
        component: RegisterComponent
     },
 

    
];

@NgModule({
    imports: [
        RouterModule.forRoot(
          appRoutes,
          { enableTracing: false } // <-- for debugging
        )
      ],
     
  exports: [ RouterModule ]
})
export class AppRoutingModule {}