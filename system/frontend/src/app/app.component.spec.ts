import { TestBed, async } from '@angular/core/testing';
import { AppComponent } from './app.component';
import { HeaderComponent } from './header/header.component';
import { ContentComponent } from './content/content.component';
import { NO_ERRORS_SCHEMA } from '@angular/core';
import { AuthService } from './auth.service';
import { HeaderService } from './header/header.service';
import { StorageService } from './storage/storage.service';
import { Router } from '@angular/router';
describe('AppComponent', () => {
  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [
        AppComponent,
        HeaderComponent,
        ContentComponent,
        
      ],
      providers:[
        AuthService,
        HeaderService,
        StorageService,
        { provide: Router,      useClass: RouterStub }
      ],
      schemas: [ NO_ERRORS_SCHEMA]
    }).compileComponents();
  }));
  it('should create the app', async(() => {
    const fixture = TestBed.createComponent(AppComponent);
    const app = fixture.debugElement.componentInstance;
    expect(app).toBeTruthy();
  }));
 
});


class RouterStub {
  navigateByUrl(url: string) { return url; }
}