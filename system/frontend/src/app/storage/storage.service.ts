import { Injectable } from '@angular/core';
import { Subject } from 'rxjs/Subject';
import { Observable } from 'rxjs/Observable';
import { User } from '../model/User';

@Injectable()
export class StorageService {


  private user:User;

  constructor() { 


  }




  public get User() {
    return this.user;
  }

  public set User(user:User) {
      //you can use webstorage if you want a permanent solution, but then you have to think about how to store it in a secure manner
      this.user = user;
  }





}
