import { Routes, RouterModule } from "@angular/router";
import { LobbyComponent } from "./content/lobby/lobby.component";
import { NgModule } from "@angular/core";
import { AuthGuard } from "./auth-guard.service";

const loginRoutes: Routes = [
    { 
        path: 'lobby', 
        component: LobbyComponent,
        canActivate: [AuthGuard],
     },
  ];
  
  @NgModule({
    imports: [
      RouterModule.forRoot(
        loginRoutes,
        { enableTracing: false } // <-- for debugging
      )
    ],
    exports: [
      RouterModule
    ]
  })
  export class loginRoutingModule {}