export class Message {
     
    constructor(
      public author:string,
      public message:string
    ) {  }

  }