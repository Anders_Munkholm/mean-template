export class SocketRequest {
    constructor(
      public message:string,
      public token:string,
      public room:string
    ) {  }

  }