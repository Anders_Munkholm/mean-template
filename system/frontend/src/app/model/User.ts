export class User {

    constructor(
      public id: number,
      public name: string,
      public lastname:   string,
      public username: string,
      public email:  string,
      public password: string,
      public token: string,
      public status: string
    ) {  }

  }