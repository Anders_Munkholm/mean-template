import { TestBed, inject } from '@angular/core/testing';

import { RegisterService } from './register.service';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { AuthService } from '../../auth.service';
import { User } from '../../model/User';

describe('RegisterService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [
        HttpClientTestingModule
      ],
      providers: [
        RegisterService,
        AuthService
      ]
    });
  });

  it('should be created', inject([RegisterService], (service: RegisterService) => {
    expect(service).toBeTruthy();
  }));


  it('should call register', inject([RegisterService], (service: RegisterService) => {
    let spy = spyOn(service, 'register')
    service.register(new User(1,"Anders","","","","","",""))
    expect(spy.calls.count()).toBe(1)
  }));


  it('should be correct Url', inject([RegisterService], (service: RegisterService) => {
    expect(service.loginUrl).toBe('http://localhost:3000/users/register');
  }));
});
