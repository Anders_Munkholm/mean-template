import { Injectable } from '@angular/core';
import { User } from '../../model/User';

import { HttpClient, HttpHeaders } from '@angular/common/http';

import { Observable } from 'rxjs/Observable';
import { of } from 'rxjs/observable/of';

const httpOptions = {
  headers: new HttpHeaders({ 'Content-Type': 'application/json' })
};

@Injectable()
export class RegisterService {

  
  public loginUrl = "http://localhost:3000/users/register";

  constructor(private http: HttpClient,) { }

  public register(user:User): Observable<any> {
    return this.http.post(this.loginUrl,user,httpOptions);
  }

}
