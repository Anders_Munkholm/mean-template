import { Component, OnInit } from '@angular/core';
import { User } from '../../model/User';
import { RegisterService } from './register.service';
import { Alert } from 'selenium-webdriver';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css'],
  providers: [RegisterService]
})
export class RegisterComponent implements OnInit {
 
  constructor(private modalService: NgbModal,private registerService: RegisterService) { }


  model = new User(1, 'Evil', 'Evilson', "username", "email", "password", "token","false");

  public onSubmit(): void {
    this.registerService.register(this.model).subscribe(res => {
      if (this.isRegistered(res)) {
        
        this.modalService.open("success");
      } else {
        this.modalService.open("username taken");
      }
    }
    );
  }

  


  ngOnInit() {
  }


  public isRegistered(res: any): boolean {
    var response =  res.status;
    return response === "success";
  }






}
