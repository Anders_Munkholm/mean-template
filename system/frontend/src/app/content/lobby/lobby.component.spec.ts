import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LobbyComponent } from './lobby.component';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { NO_ERRORS_SCHEMA } from '@angular/core';
import { StorageService } from '../../storage/storage.service';
import { Router } from '@angular/router';
import { Room } from '../../model/Room';


describe('LobbyComponent', () => {
  let component: LobbyComponent;
  let fixture: ComponentFixture<LobbyComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports:[NgbModule.forRoot()],
      declarations: [ LobbyComponent ],
      providers:[
        StorageService,
        { provide: Router,      useClass: RouterStub }
      ],
      schemas: [ NO_ERRORS_SCHEMA],
      
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LobbyComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });


  it('should have no rooms', () => {
    expect(component.hasNoRoom()).toBeTruthy();
  });

  it('should have rooms', () => {
    let arr = new Array<Room>();
    arr.push(new Room("dd",""))
    component.lobbyChannels = arr;
    component.newRoom = "dd";
    expect(component.hasNoRoom()).toBeFalsy();
  });
});

class RouterStub {
  constructor() {

  }
  navigateByUrl(url: string) { return url; }
  navigate(url: any) { return url; }
}