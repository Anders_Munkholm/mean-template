import { Component, OnInit } from '@angular/core';
import { StorageService } from '../../storage/storage.service';
import { OnDestroy } from '@angular/core/src/metadata/lifecycle_hooks';
import { LobbyService } from './lobby.service';
import { Router } from '@angular/router';
import { Room } from '../../model/Room';
@Component({
  selector: 'app-lobby',
  templateUrl: './lobby.component.html',
  styleUrls: ['./lobby.component.css'],
  providers: [LobbyService]
})
export class LobbyComponent implements OnInit, OnDestroy {
  public generalMessage: string;
  public error: string;
  public username: string;
  public joinedChannels: string[] = [];
  public lobbyChannels: Room[] = [];
  public newRoom: any = "";


  constructor(private storageService: StorageService, private lobbyService: LobbyService, private router: Router) { }

  ngOnInit() {
    if (this.storageService.User != null) {
      this.lobbyService.listenMessages().subscribe(message => {

        if (message.includes('you have joined the lobby!')) {
          this.generalMessage = message;

        } else if (message.includes('channel-o')) {
          this.joinedChannels.push(this.extractChannel(message));

        } else if (message.includes('channel--')) {
          var index = this.joinedChannels.indexOf('channel--', 0);

          if (index > -1) {
            this.joinedChannels.slice(index);

          }
        }
      })
      this.lobbyService.newRoomStream().subscribe(room => {
        this.lobbyChannels.push(room)
      });

      this.lobbyService.listenChatRooms().subscribe(chatrooms => {
        this.lobbyChannels = chatrooms;
      })
      this.lobbyService.errorStream().subscribe(
        error => {
          this.error = error;
        })
      this.username = this.storageService.User.username;
      this.lobbyService.joinLobby();
    } else {
      this.router.navigate(["/login"]);
    }
  }


  createRoom() {
    if (this.newRoom.length > 0 && this.newRoom != 'join' && this.newRoom != 'chatRooms' && this.newRoom != 'joinChat' && this.newRoom != 'error' && this.newRoom != 'newMessage' && this.newRoom != 'newChatRoom') {
      if (this.hasNoRoom()) {
        console.log("enter")
        this.lobbyChannels.push(new Room(this.newRoom,""))
        this.lobbyService.joinChat(this.newRoom)
      } else { 
        console.log("enter2")
      }
    }

  }

  joinRoom(name:string) {
    this.lobbyService.joinChat(name)
  }

  public hasNoRoom() : boolean{
    var result = true;
    this.lobbyChannels.forEach(room => {
      var r = new Room(this.newRoom,"");
      if (room.name === r.name) {
        result = false;

      }
    })
console.log(result)
    return result;
  }



  getSocket(): any {
    return this.lobbyService.getSocket();
  }

  extractChannel(roomData: string): string {
    var index = roomData.indexOf('-o');
    return roomData.substring(index + 2);
  }

  ngOnDestroy(): void {
    if (this.storageService.User != null) {
      this.lobbyService.killConnection();
    }
  }



}
