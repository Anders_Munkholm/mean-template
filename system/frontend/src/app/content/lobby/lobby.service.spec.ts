import { TestBed, inject } from '@angular/core/testing';

import { LobbyService } from './lobby.service';
import { StorageService } from '../../storage/storage.service';

describe('LobbyService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [LobbyService,StorageService]
    });
  });

  it('should be created', inject([LobbyService], (service: LobbyService) => {
    expect(service).toBeTruthy();
  }));


  it('should listen to chatRooms', inject([LobbyService], (service: LobbyService) => {
    
    var result = "false";
    service.listenChatRooms().subscribe(event => {result = event})
    service.roomSubject.next("ddd")
    expect(result).toBe("ddd");
  }));


  it('should listen to newRooms', inject([LobbyService], (service: LobbyService) => {
    
    var result = "false";
    service.newRoomStream().subscribe(event => {result = event})
    service.newRoomSubject.next("ddd")
    expect(result).toBe("ddd");
  }));


  it('should listen to new chatrooms that we should join, it has the channel-o prefix', inject([LobbyService], (service: LobbyService) => {
    
    var result = "false";
    service.listenMessages().subscribe(event => {result = event})
    service.listenChat("ddd")
    expect(result).toBe("channel-oddd");
  }));
});
