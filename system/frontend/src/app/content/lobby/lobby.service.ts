import { Injectable } from '@angular/core';
import * as io from 'socket.io-client';
import { StorageService } from '../../storage/storage.service';
import { Subject } from 'rxjs/Subject';
import { Observable } from 'rxjs/Observable';
import { SocketRequest } from '../../model/SocketRequest';
@Injectable()
export class LobbyService {
  public socket: any;
  public subject: Subject<string> = new Subject<string>();
  private observable: Observable<string> = this.subject.asObservable();

  public errorSubject: Subject<string> = new Subject<string>();
  private errorObserver: Observable<string> = this.errorSubject.asObservable();

  public roomSubject: Subject<string> = new Subject<string>();
  private roomObserver: Observable<string> = this.roomSubject.asObservable();


  public newRoomSubject: Subject<any> = new Subject<any>();
  private newRoomObserver: Observable<any> = this.newRoomSubject.asObservable();


  constructor(private storageService: StorageService) {

  }



  joinLobby() {
    var socket = this.createSocket();
    this.socket.on('connect', () => {
      this.socket.emit('join', this.storageService.User.token);
    })
    //listens if we are authenticated
    this.socket.on('authenticated', (data) => {
      if (('' + data).includes('welcome')) {
        this.subject.next('you have joined the lobby!')
        this.joinChat('roomAwesome');
      }
    });
    //listens for the channel that we create/join
    this.socket.on('joinedChat', (data) => {
      this.listenChat(data);

    })

    this.socket.on('error', (data) => {
      this.errorSubject.next(data);
    })

    //listening for new chat rooms and getting all chat rooms
    this.socket.on('chatRooms', (data) => {
      this.roomSubject.next(data);
    });


    this.socket.on('newChatRooms', (data) => {
      this.newRoomSubject.next(data)
    });
    this.getRooms();

  }


  public newRoomStream(): Observable<any> {
    return this.newRoomObserver;
  }

  listenChatRooms(): Observable<any> {
    return this.roomObserver;
  }

  public errorStream(): Observable<string> {
    return this.errorObserver;
  }

  getRooms() {
    this.socket.emit('chatRooms', this.storageService.User.token)
  }

  //joins a chat room and starts listening
  listenChat(chat: string) {
    this.subject.next('channel-o' + chat);
  }

  //returns only the relevant messages back from the server, not errors
  listenMessages(): Observable<string> {
    return this.observable;
  }

  joinChat(roomName: string) {
    this.socket.emit('joinChat', new SocketRequest(roomName,this.storageService.User.token,""))
  }




  createSocket(): any {
    return this.socket = io.connect('http://localhost:3100');
  }


  authenticate(data: any) {
    console.log(this.socket)
    this.socket.emit('join', this.storageService.User.token);
  }

  getSocket(): any {
    return this.socket;
  }

  killConnection() {
    this.socket.disconnect();
    this.socket = null;
  }

}
