import { Component, OnInit, Input } from '@angular/core';
import { ChatService } from './chat.service';
import { SocketRequest } from '../../../model/SocketRequest';
import { StorageService } from '../../../storage/storage.service';
import { Message } from '../../../model/Message';
import { OnDestroy } from '@angular/core/src/metadata/lifecycle_hooks';

@Component({
  selector: 'app-chat',
  templateUrl: './chat.component.html',
  styleUrls: ['./chat.component.css'],
  providers: [ChatService]
})
export class ChatComponent implements OnInit, OnDestroy {

  @Input()
  socket: any
  @Input()
  room: string
  oldMessages: Array<Message> = new Array<Message>()
  messages: Array<Message> = new Array<Message>();
  message: string;


  constructor(private chatService: ChatService, private storageService: StorageService) { }



  startChat() {
    this.chatService.getOldMessages(this.socket, this.room).subscribe(
      messages => {
        this.oldMessages = messages;
      }
    );
    this.chatService.listenChat(this.socket, this.room).subscribe(
      message => {
        this.messages.push(message)
      }
    );
  }

  sendMessage() {
    this.chatService.sendChatMessage(
      this.message,
      this.room,
      this.storageService.User.token, this.socket)
    this.messages.push(new Message(this.storageService.User.username,this.message, ))
    this.message = "";
  }

  ngOnInit() {
    this.startChat()
  }

  ngOnDestroy() {
    this.chatService.stopListening(this.socket, this.room)
  }

}
