import { Injectable } from '@angular/core';
import * as io from 'socket.io-client';

import { Subject } from 'rxjs/Subject';
import { Observable } from 'rxjs/Observable';
import { StorageService } from '../../../storage/storage.service';
import { SocketRequest } from '../../../model/SocketRequest';
import { Message } from '../../../model/Message';

import { HttpClient, HttpHeaders } from '@angular/common/http';

import { of } from 'rxjs/observable/of';


const httpOptions = {
  headers: new HttpHeaders({ 'Content-Type': 'application/json' })
};


@Injectable()
export class ChatService {

  public chatUrl = "http://localhost:3000/chat/old";
  constructor(private storageService: StorageService, private http: HttpClient) { }


  public listenChat(socket: any, chat): Observable<Message> {
    let observable = new Observable<Message>(observer => {
      socket.on(chat, (message) => {
        observer.next(message)
      })
      //to disconnect from the chat
    });
    return observable;
  }

  public getOldMessages(socket: any, room): Observable<Message[]> {
    var message = new SocketRequest(room, this.storageService.User.token, room)
    return this.http.post<Message[]>(this.chatUrl, message, httpOptions);
  }





  public sendChatMessage(message: string, room: string, token: string, socket: any) {
    var request = new SocketRequest(message, token, room);
    socket.emit("newMessage", request)
  }


  public stopListening(socket: any, room: string) {
    socket.removeListener(room)
  }



}
