import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { FormsModule } from '@angular/forms';
import { ChatComponent } from './chat.component';
import { StorageService } from '../../../storage/storage.service';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { ChatService } from './chat.service';
import { Observable } from 'rxjs/Observable';
import { Message } from '../../../model/Message';
describe('ChatComponent', () => {
  let component: ChatComponent;
  let fixture: ComponentFixture<ChatComponent>;
  let httpMock: HttpTestingController;

  let storageService =
    beforeEach(async(() => {
      TestBed.configureTestingModule({
        imports: [FormsModule,
          HttpClientTestingModule
        ],
        declarations: [ChatComponent],
        providers: [
          StorageService,

          { provide: ChatService, useclass: MockChatService }
        ]
      })

        .compileComponents();
      storageService = TestBed.get(StorageService);
      httpMock = TestBed.get(HttpTestingController);
    }));



  beforeEach(() => {
    fixture = TestBed.createComponent(ChatComponent);
    component = fixture.componentInstance;
    spyOn(component, 'startChat').and.callFake(
      function() {
        
      }
    )
    fixture.detectChanges();
  });



  it('should create', () => {
  
    expect(component).toBeTruthy();

    httpMock.verify();
  });
});


@Injectable()
export class MockChatService {

  private chatUrl = "http://localhost:3000/chat/old";
  constructor(private storageService: StorageService, private http: HttpClient) { }
  public getOldMessages(socket: any, room): Observable<Message[]> {
    let arr = new Array<Message>();
    arr.push(new Message("222","222"))
    return Observable.of(arr);
  }

}





