import { TestBed, inject } from '@angular/core/testing';

import { ChatService } from './chat.service';
import { Injectable } from '@angular/core';
import { StorageService } from '../../../storage/storage.service';
import { HttpClientTestingModule } from '@angular/common/http/testing';

describe('ChatService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [
        HttpClientTestingModule
      ],
      providers: [ChatService,StorageService]
    });
  });

  it('should be created', inject([ChatService], (service: ChatService) => {
    expect(service).toBeTruthy();
  }));

  it('should have the corecct url', inject([ChatService], (service: ChatService) => {
    expect(service.chatUrl).toBe("http://localhost:3000/chat/old");
  }));
});

