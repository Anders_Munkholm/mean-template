import { Component, OnInit } from '@angular/core';
import { LoginService } from './login.service';
import { User } from '../../model/User';


@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css'],
  providers:[LoginService]
})
export class LoginComponent implements OnInit {

  constructor(private loginService: LoginService) { }


  model = new User(1, 'Dr IQ', 'Chuck Overstreet',"username","email","password","token","false");

  public onSubmit():void {
    this.loginService.login(this.model)
  }

  



  ngOnInit() {
  }

}
