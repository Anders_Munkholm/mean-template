import { Injectable } from '@angular/core';
import { User } from '../../model/User';
import { HttpClient, HttpHeaders } from '@angular/common/http';

import { Observable } from 'rxjs/Observable';
import { of } from 'rxjs/observable/of';
import { StorageService } from '../../storage/storage.service';
import { Router } from '@angular/router';
import { HeaderService } from '../../header/header.service';
import { AuthService } from '../../auth.service';

const httpOptions = {
  headers: new HttpHeaders({ 'Content-Type': 'application/json' })
};


@Injectable()
export class LoginService {

  public loginUrl = "http://localhost:3000/users/login";

  constructor(private authService:AuthService,private http: HttpClient,private storageService:StorageService,private router:Router,private headerService:HeaderService) { }


  public login(user:User) {
    this.authService.login(this.loginUrl,user,httpOptions,this.http).subscribe(user2 => {
      if (this.authService.isLoggedIn) {
        // Get the redirect URL from our auth service
        // If no redirect has been set, use the default
        let redirect = this.authService.redirectUrl ? this.authService.redirectUrl : '/lobby';
        this.storageService.User = user2;
        this.headerService.changeLoginHeader(true);
        // Redirect the user
        this.router.navigate([redirect]);
      } else {
        alert("error in password/username")
      }
    });
  }

}
