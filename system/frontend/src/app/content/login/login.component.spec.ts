import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LoginComponent } from './login.component';
import { FormsModule } from '@angular/forms';
import { AuthService } from '../../auth.service';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { StorageService } from '../../storage/storage.service';
import { Router } from '@angular/router';
import { HeaderService } from '../../header/header.service';
import { User } from '../../model/User';

describe('LoginComponent', () => {
  let component: LoginComponent;
  let fixture: ComponentFixture<LoginComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [FormsModule,
        HttpClientTestingModule
      ],
      providers:[
        StorageService,
        AuthService,
        HeaderService,
        { provide: Router,      useClass: RouterStub }
      ],
      declarations: [ LoginComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LoginComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });


  it('should call login on submit', () => {
    let spy = spyOn(component, 'onSubmit')
    component.onSubmit()
    expect(spy.calls.count()).toBe(1);
  });

  it('should be correct model', () => {
    let model =  new User(1, 'Dr IQ', 'Chuck Overstreet',"username","email","password","token","false");
    expect(component.model.id).toBe(1)
    expect(component.model.name).toBe('Dr IQ')
    expect(component.model.lastname).toBe('Chuck Overstreet')
    expect(component.model.username).toBe("username")
    expect(component.model.email).toBe("email")
    expect(component.model.password).toBe("password")
    expect(component.model.token).toBe("token")
    expect(component.model.status).toBe("false")
  });


});

class RouterStub {
  navigateByUrl(url: string) { return url; }
}