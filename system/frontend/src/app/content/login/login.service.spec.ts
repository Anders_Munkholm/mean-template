import { TestBed, inject } from '@angular/core/testing';

import { LoginService } from './login.service';
import { AuthService } from '../../auth.service';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { StorageService } from '../../storage/storage.service';
import { Router } from '@angular/router';
import { HeaderService } from '../../header/header.service';

describe('LoginService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [
        HttpClientTestingModule
      ],
      providers: [
        LoginService,
        AuthService,
      StorageService,
      HeaderService,
      { provide: Router,      useClass: RouterStub }
    ]
    });
  });

  it('should be created', inject([LoginService], (service: LoginService) => {
    expect(service).toBeTruthy();
  }));


  it('should be the correct url', inject([LoginService], (service: LoginService) => {
    expect(service.loginUrl).toBe('http://localhost:3000/users/login');
  }));

 
});


class RouterStub {
  navigateByUrl(url: string) { return url; }
}
