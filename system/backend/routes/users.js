var express = require('express');
var router = express.Router();
var registerService = require('../service/users/register.service');
var loginService = require('../service/users/login.service');

/* GET users listing. */

router.post('/register', registerService.register);


router.post('/login', loginService.login);








module.exports = router;
