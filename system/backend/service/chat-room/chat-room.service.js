
var express = require('express');
var mongoose = require('mongoose');
var chatRoomSchema = require('../../model/chatroom.schema');
var userSchema = require('../../model/user.schema');
var messageSchema = require('../../model/message.schema');


exports.notificationRooms = function notificationRooms(data, socket) {

    var token = data;

    userSchema.User.findOne({ token: token }).exec(function (err, user) {
        if (user != null) {
            chatRoomSchema.ChatRoom.find({}).exec(function (err, chatRooms) {
                if (chatRooms != null) {
                    socket.emit('chatRooms', chatRooms)
                }
            });
        } else {
            socket.emit("error", "-oauthError")
        }
    });
}

exports.joinChat = function joinChat(data, socket) {
    var token = data.token;
    var message = data.message;//message is a room


    userSchema.User.findOne({ token: token }).exec(function (err, user) {
        if (user != null) {
            
            socket.join(message)
            chatRoomSchema.ChatRoom.findOne({ name: message }).exec(function (err, chatRooms) {
                
                if (chatRooms == null) {
                    var room = new chatRoomSchema.ChatRoom
                    room.name = message
                    room.messages.push()
                    room.save();
                    socket.broadcast.emit('newChatRooms', {message: 'has been created'  ,  name:message})
                    console.log('room created')
                    
                    
                } 
                console.log("client joined chat")
                socket.emit("joinedChat", message)
                socket.broadcast.to(message).emit(message, {   "author":'system',     "message":user.username + " joined the chat"})

            });

        } else {
            socket.emit("error", "-oauthError")
        }
    });
}



exports.sendMessage = function sendMessage(data, socket) {
    userSchema.User.findOne({ token: data.token }).exec(function (err, user) {
        if (user != null) {
            var message = new messageSchema.Message
            message.author = user.username
            message.message = data.message
            chatRoomSchema.ChatRoom.findOneAndUpdate({ name:data.room }, {$push: {messages: message}}).exec(function (err, chatroom) {
                if (chatroom != null) {
                    socket.broadcast.to(data.room).emit(data.room, message)
                } else {
                    socket.emit("error", "no room with that name exist anymore")
                }
            })
        } else {
            socket.emit("error", "-oauthError")
        }
    });
}

exports.getOldMessages = function getOldMessages(req, res, next) {
   
    var token = req.body.token;
    var message = req.body.message;

    userSchema.User.findOne({ token: token }).exec(function (err, user) {
        if (user != null) {
            chatRoomSchema.ChatRoom.findOne({ name:message }).exec(function (err, chatroom) {
                if (chatroom != null) {
                   res.send(chatroom.messages)
                } else {
                    res.send('error')
                }
            })
        } else {
            res.send('error')
        }
    });
}


