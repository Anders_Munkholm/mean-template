var express = require('express');
var mongoose = require('mongoose');
var userSchema = require('../../model/user.schema');
var bcrypt = require('bcrypt');

exports.login = function (req, res, next) {
    var username = req.body.username;
    userSchema.User.findOne({username:username}).
    exec(function (err, user) {
        if (err) return handleError(err);
        if (user != null) {
            validatePassword(req,res,next,user.password,user);
        } else {
            res.send('{ "status" : "username/password missmatch"}')
        }
   


    });
   
}


function validatePassword(req,res,next,hash,user) {

    
    var password = req.body.password;
    bcrypt.compare(password, hash, function(err, ress) {
        // res == true
        if (ress) {
            user.token = createToken();
            user.save();

            res.send('{"status":"success",'+
            '"token":"'+ user.token +'",'+
            '"username":"'+ user.username +'",'+
            '"name":"'+ user.name +'",'+
            '"lastname":"'+user.lastname +'",'+
            '"email":"' + user.email +'"'
            +'}')
            
        } else {
            res.send('{ "status" : "username/password missmatch"}')
        }
    });
}

var createToken =  function createToken() {
    var token = "";
    var characters = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
    
    for (var i = 0; i < 64; i++)
    token += characters.charAt(Math.floor(Math.random() * characters.length));
  
    return token;
  }

  exports.createToken = createToken;