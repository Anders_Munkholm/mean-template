var express = require('express');
var mongoose = require('mongoose');
var userSchema = require('../../model/user.schema');
var bcrypt = require('bcrypt');
const saltRounds = 10;

exports.register = function register(req, res, next) {
    var username = req.body.username;

    userSchema.User.findOne({ username: username }).
        exec(function (err, userCount) {
            if (err) return handleError(err);
            if (userCount == null) {
                createUser(req,res,next)
            } else {
                res.send(success())
            }
        });
}



function createUser(req,res,next) {

    var name = req.body.name;
    var lastname = req.body.lastname;
    var email = req.body.email;
    var username = req.body.username;
    var password = req.body.password;

    bcrypt.genSalt(saltRounds, function (err, salt) {
        bcrypt.hash(password, salt, function (err, hash) {

            var u = new userSchema.User
            u.name = name;
            u.lastname = lastname;
            u.username = username
            u.email = email;
            u.password = hash;
            u.save();
            res.send(success());
            // Store hash in your password DB.
        });
    });
}



var success = function success() {
    return '{"status":"success"}';
}

exports.success = success