

var bcrypt = require('bcrypt');
const saltRounds = 10;
var userSchema = require("../model/user.schema");
var chatRoomSchema = require("../model/chatroom.schema");
var messageSchema = require("../model/message.schema");


exports.seeding = function() {

    console.log("checking for created tables...")
    usersExist();
    chatRoomExist();


}


function chatRoomExist() {
    chatRoomSchema.ChatRoom.count({name: 'roomAwesome'}).
    exec(function (err, userCount) {
       if (err) return handleError(err);
       if (userCount <= 0) {
           console.log('inserting rooms')
           insertChatRooms();
           console.log('rooms inserted')
       } else {
           console.log('rooms exist')
       }
    });
}

// messages: [{
//     message:String,
//     author:String//this is the username
// }]

function insertChatRooms() {
    
    var message1 = new messageSchema.Message
    message1.message = "hello test object";
    message1.author = "anders";
    var message2 = new messageSchema.Message
    message2.message = "hello long textjdalksdjlk";
    message2.author = "anders";

    var message3 = new messageSchema.Message
    message3.message = "hello test object";
    message3.author = "anders";
    message3.save();

    var room = new chatRoomSchema.ChatRoom
    room.name = "roomAwesome";
    room.messages.push(message2,message1)
    room.save();
}

function usersExist() {
     userSchema.User.count({name: 'anders'}).
     exec(function (err, userCount) {
        if (err) return handleError(err);
        if (userCount <= 0) {
            console.log('inserting missing users')
            insertData();
            console.log('users inserted')
        } else {
            console.log('users exist')
        }
     });
    
}

function validateTableContent() {
    return true;
}


function insertData() {
    var myPlaintextPassword = "123";

    bcrypt.genSalt(saltRounds, function(err, salt) {
        bcrypt.hash(myPlaintextPassword, salt, function(err, hash) {
            
            var u = new userSchema.User
            u.name = "anders";
            u.lastname = "munkholm";
            u.username = "and"
            u.email = "a@c";
            u.password = hash;
            u.save();

            // Store hash in your password DB.
        });
    });
    




}