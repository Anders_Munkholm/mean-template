var mongoose = require('mongoose');
var exports = module.exports = {};
var seed = require('./seed');

//Connect to database
mongoose.Promise = global.Promise; 

//or mongoose.connect('mongodb://127.0.0.1:27017/appname') to get the right port and name
mongoose.connect('mongodb://localhost/dataEksamen', {useMongoClient: true});
exports.db = mongoose.connection;
exports.db.on('error', console.error.bind(console, 'connection error:'));
exports.db.once('open', function() {
    console.log("we're connected to database through mongoose!");
    console.log("starting the seed function");
    seed.seeding();
});


