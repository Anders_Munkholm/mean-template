
var chatRoomSchema = require('../model/chatroom.schema')
var server = null;
var io = null;
var chatRoomService = require('../service/chat-room/chat-room.service');
var userSchema = require('../model/user.schema');


exports.startSocketServer = function startSocketServer(app) {

    server = require('http').createServer(app);
    io = require('socket.io')(server);

    server.listen(3100);
}


exports.initSockets = function initSockets() {
    //setting up the route for the socket
    io.on('connection', function (socket) {
        console.log('Client connected...');

        //if we join a channel we emit a message back
        socket.on('join', function (data) {
            var data = data;
            authenticate(data, socket);
        });

        //returns all chatrooms if requested, 
        //returns newly created chatrooms
        socket.on('chatRooms',function (data) {
            chatRoomService.notificationRooms(data, socket)
        });

        //we listen for chat rooms
        socket.on('joinChat', function (data) {
          
            chatRoomService.joinChat(data, socket)
        });

        

        socket.on('newMessage',function (data) {
            chatRoomService.sendMessage(data, socket)
        });

        socket.on('newChatRooms',function (data) {
            //nothing
         });
        socket.on('error',function (data) {
           //nothing
        });
    });
}



function extractToken(data) {
    var string = '' + data;
    var index = string.indexOf('-o')
    return string.substring(index+2)
}

function extractMessage(data) {
    var string = '' + data;
    var index = string.indexOf('-o')
    return string.substring(0,index)
}


function authenticate(data, client)  {
    userSchema.User.findOne({token: data}).exec(function (err, user) {
        if (user != null) {
            client.emit('authenticated', '-¤welcome ' + user.username);
        } else {
            console.log()
            client.emit('error', '-¤autherror');
        }
    });
}
