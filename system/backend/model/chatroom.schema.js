var mongoose = require('mongoose');
var Schema = mongoose.Schema;
var exports = module.exports = {};
var messageSchema = require('./message.schema');

exports.chatRoomSchema = new Schema({
    name: String,
    messages: [messageSchema.messageSchema]
});
exports.ChatRoom = mongoose.model('chatRoom',exports.chatRoomSchema);