var mongoose = require('mongoose');
var Schema = mongoose.Schema;
var exports = module.exports = {};

exports.userSchema = new Schema({
    name: String,
    lastname:   String,
    username: String,
    email:  String,
    password: String,
    token: String
});
exports.User = mongoose.model('User',exports.userSchema);