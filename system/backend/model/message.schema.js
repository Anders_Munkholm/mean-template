var mongoose = require('mongoose');
var Schema = mongoose.Schema;
var exports = module.exports = {};

exports.messageSchema = new Schema({
    message: String,
    author:String
});

exports.Message = mongoose.model('message',exports.messageSchema);