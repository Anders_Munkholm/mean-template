

var chai = require('chai');
var expect = chai.expect
var loginService = require('../service/users/login.service')
var registerService = require('../service/users/register.service')

var reqMock = {
    body: {
        name: 'anders',
        lastname: 'da',
        email: 'da',
        username: 'da',
        password: 'da'
    }
}


var result = "";
var resMock = function () {

}

var nextMoch = function() {

}
var resMock = {
    result: "John",
    send : function(string) {
        this.result = string;
       return this.result;
    },
    getResult: function(string) {
        
        return this.result;
     }
};


describe('loginService', function () {
    describe('createToken()', function () {
        it("should return a string", function () {
            expect(loginService.createToken()).to.be.an('string')
        });
        it("should return a 64 long string", function () {
            expect(loginService.createToken().length).to.equal(64);
        });
    });


    describe('success()', function () {
        it('should contain json success', function () {
            expect(registerService.success()).to.equal('{"status":"success"}')
        })
    });
});
